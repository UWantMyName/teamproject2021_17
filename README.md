# TeamProject2021_17 

Welcome to the restaurant management tool built by Team 17. Below are the steps that you have to follow to set up your person on device MySQL server so the application will work.
Also the way to run the application itself after the setup of the Database server.
The contributors of this project are also listed below. 

MySQL Database setup steps on MySQL workbench:
  1) Download MySQL and MySQL work bench from https://www.mysql.com/downloads/
  2) Open MySQL workbench. Next to the section header named MySQL connections press the add button which is to the right of the section header.
  3) You will now see a form to create a new connection. Set the connection name to be called restaurante. Set the Username to root and the Password to student. Keep the rest of
  the fields as default and press OK.
  4) A MySQL connection named restaurante should now have appeared underneath the MySQL connections section header. Double click on the connection and the connection restaurante 
  should now open on the work bench. From here we are going to add a new schema to the connection which is also to be called restaurante. To do this go to the icons at the top
  of the screen. Here you should be able to see a icon that looks like a cylinder with lines crossing it. This should be the icon 4 from the left and if you hover over it the
  message "Create a new schema in the connected server" should appear. Now double click on this icon. A tab should open called new_schema and the corresponding form there is a
  "Name:" field. Within the Name: field type "restaurante", keep all the other fields as their defaults and double click apply at the bottom of the form.
  5) Finally, we have to check that the MySQL server is running. This is done by using the leftmost panel called navigator. There should be a tab at the bottom of this panel
  with the choices of Schemas and Administration. Click on Administration. The navigator panel should now have 3 sections one of which is instance. Within this the first option 
  is Startup/Shutdown, double click on this. A tab should now open where you can check if the server running as there would be a green "running" word within the tab. If not then
  press on the start server button.
  6) That is the server set up enjoy!
  
To Run The app:   After setting up MySQL Workbench-
  1) Download and open the project and go into the appcontroler package and into the appMain.java file.
  2) Run the application as "java application" or "spring boot app" .
  3) Go into your web browser(preferably google chrome as it was the primary search engine used).
  4) In the search bar type " http://localhost:8080/ " and the website should be accessible.
  5) Enjoy our Oaxaca Website.

For a video on the functionality of the website visit - https://youtu.be/cihc30iCkQo

Contributors:
  1) Stefan Tanasa
  2) Seba Durlak
  3) Sean Grady
  4) Valerie Song
  5) Shreyas Panditrao
  6) Molly Howard
  7) Casey Hawkins
 
Last Update of the ReadMe - 25/03/2021
