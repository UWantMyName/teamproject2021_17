package staffmanager;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Object that contains the Staff that the user has made.
 * 
 * @author Sean, Valerie, Shreyas, Stefan, Molly
 */

@Entity
public class Staff {
	private int staffID;
	private String name;
	private String role;
	private String onduty;

	/**
	 * Empty Constructor for the Staff object.
	 */
	public Staff() {
	}

	/**
	 * Constructor of staff object.
	 * 
	 * @param staffID id of staff
	 * @param name    the name of staff
	 * @param role    the responsibility of the staff
	 * @param onduty  the staff on duty
	 */
	public Staff(int staffID, String name, String role, String onduty) {
		super();
		this.staffID = staffID;
		this.name = name;
		this.role = role;
		this.onduty = onduty;
	}

	/**
	 * Gets the staff id.
	 * 
	 * @return Integer id of the staff
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getStaffID() {
		return staffID;
	}

	/**
	 * Sets staff ID.
	 * 
	 * @param staffID id of staff.
	 */
	public void setStaffID(int staffID) {
		this.staffID = staffID;
	}

	/**
	 * Gets the name of the staff.
	 * 
	 * @return String name of staff
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets staff name.
	 * 
	 * @param name of staff
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the role of the staff.
	 * 
	 * @param role of the staff
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * Gets the role of the staff.
	 * 
	 * @return String role of the staff
	 */
	public String getRole() {
		return role;
	}

	/**
	 * Sets which staff is on duty.
	 * 
	 * @param onduty represents the staff on duty
	 */
	public void setOnduty(String onduty) {
		this.onduty = onduty;
	}

	/**
	 * Gets the staff on duty.
	 * 
	 * @return String of staff on duty
	 */
	public String getOnduty() {
		return onduty;
	}
}