package staffmanager;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Creates repository object to interface with JPArepo that stores the users Orders.
 * @author Shreyas
 */
public interface StaffRepository extends JpaRepository<Staff, Integer> {
}
