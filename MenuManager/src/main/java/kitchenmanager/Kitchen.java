package kitchenmanager;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Kitchen object to store the dishes on order. 
 * @author Shreyas, Sean, Seb
 */
@Entity
public class Kitchen {
	private int kitchenID;
	private int orderID;
	private int tablenum;
	private String description;
	private String status;
	private Time ordertime;
	
	/**
	 * Empty Constructor for the Kitchen object.
	 */
	public Kitchen() {
	}

	/**
	 * Constructor of menu object.
	 * @param orderID id of the order 
	 * @param tablenum tablenum of the order
	 * @param description brief explanation of the order
	 * @param status the status of the order
	 * @param ordertime ordertime in GBP of the order
	 */
	public Kitchen(int orderID, int tablenum, String description, String status, Time ordertime) {

		super();
		this.orderID = orderID;
		this.tablenum = tablenum;
		this.description = description;
		this.status = status;
		this.ordertime = ordertime;

	}
	/**
	 * Getter for ID of kitchen object.
	 * @return Integer id of kitchen object.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getKitchenID() {
		return kitchenID;
	}

	/**
	 * Setter for ID of kitchen object.
	 * @param kitchenID id of kitchen item.
	 */
	public void setKitchenID(int kitchenID) {
		this.kitchenID = kitchenID;
	}
	/**
	 * Getter for ID of kitchen object.
	 * @return Integer id of order.
	 */
	public int getOrderID() {
		return orderID;
	}

	/**
	 * Setter for ID of order.
	 * @param orderID id of order.
	 */
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	/**
	 * Getter for the tablenum of order.
	 * @return String tablenum of order
	 */
	public int getTablenum() {
		return tablenum;
	}

    /**
     * Setter for the tablenum of order.
     * @param tablenum of order
     */
	public void setTablenum(int tablenum) {
		this.tablenum = tablenum;
	}

	/**
	 * Getter for the description of dishes in order.
	 * @return String description of dishes in order
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Setter for the description of dishes in order.
	 * @param description of dishes in order
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Getter for the status of order.
	 * @return String status associated with order
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Setter for the status of order.
	 * @param status status associated with order
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * Getter for the order time of order.
	 * @return TimeStamp associated with order
	 */
	public Time getOrdertime() {
		return ordertime;
	}

	/**
	 * Setter for the order time of order.
	 * @param ordertime TimeStamp associated with item
	 */
	public void setOrdertime(Time ordertime) {
		this.ordertime = ordertime;
	}

}
