package kitchenmanager;

import org.springframework.data.jpa.repository.JpaRepository;

import kitchenmanager.Kitchen;


/**
 * Creates repository object to interface with JPArepo that stores the kitchen items.
 * @author Sean, Seb
 */
public interface KitchenRepository extends JpaRepository<Kitchen, Integer> {

}


