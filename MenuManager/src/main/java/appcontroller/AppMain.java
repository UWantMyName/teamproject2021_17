package appcontroller;

import java.sql.SQLException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import databasemanager.dbRestaurant;

/**
 * The startup for the application that initiates the database and runs the main
 * class to start the local host web-site.
 * 
 * @author sebas
 *
 */
@SpringBootApplication
@ComponentScan({ "appcontroller", "databasemanager", "kitchenmanager", "menumanager", "ordersmanager", "tablesmanager",
		"stockmanager", "staffmanager", "salesmanager" })
@EntityScan({ "appcontroller", "databasemanager", "kitchenmanager", "menumanager", "ordersmanager", "tablesmanager",
		"stockmanager", "staffmanager", "salesmanager" })
@EnableJpaRepositories({ "appcontroller", "databasemanager", "kitchenmanager", "menumanager", "ordersmanager",
		"tablesmanager", "stockmanager", "staffmanager", "salesmanager" })
public class AppMain {

	/**
	 * main class for the appmain that will run the database initiator and the main
	 * application class that will control the web-site
	 * 
	 * @param args -----
	 */
	public static void main(String[] args) {
		try {
			new dbRestaurant().main(args);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		SpringApplication.run(AppMain.class, args);
	}
}
