package appcontroller;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import kitchenmanager.Kitchen;
import kitchenmanager.KitchenRepository;
import menumanager.Menu;
import menumanager.MenuRepository;
import ordersmanager.Orders;
import ordersmanager.OrdersRepository;
import salesmanager.Sales;
import salesmanager.SalesRepository;
import staffmanager.Staff;
import staffmanager.StaffRepository;
import stockmanager.Stock;
import stockmanager.StockRepository;
import tablesmanager.Tables;
import tablesmanager.TablesRepository;

/**
 * Controller using the spring and thymeleaf framework to connect java back end
 * to the html front end.
 * 
 * @author Sean, Shreyas, Stefan, Molly
 */
@Controller
public class AppController {

	/**
	 * Instance of services to connect with their repositories to allow function for
	 * the application.
	 */
	@Autowired
	private MenuRepository menurep;
	@Autowired
	private OrdersRepository ordersrep;
	@Autowired
	private KitchenRepository kitchenrep;
	@Autowired
	private TablesRepository tablesrep;
	@Autowired
	private StockRepository stockrep;
	@Autowired
	private StaffRepository staffrep;
	@Autowired
	private SalesRepository salesrep;

	// Instance variables
	ArrayList<String> currentorders = new ArrayList<String>();
	ArrayList<String> currentorderprices = new ArrayList<String>();
	HashMap<String, Integer> orderitems;
	StockRepository stock;
	HashMap<String, Integer> recipeitems;
	HashMap<Orders, HashMap<String, Integer>> stockrequirement;
	HashMap<String, Integer> ordercount = new HashMap<String, Integer>();
	HashMap<Orders, String> timetaken = new HashMap<Orders, String>();

	// Restaurant ingredient variables for the manager overview functionity
	boolean instock;
	boolean countstored = false;
	String ingredientname;

	// URL for the images shown in the menu
	private final String UPLOAD_DIR = "src/main/resources/static/images/food/";

	// Instance used during ordering and order tracking and its effect on the
	// restaurant stock
	Orders neworder;
	Orders currentOrder;
	Stock newstock;

	// Variables used to store counts of a certain variables
	int ingredient = 0;
	int quantity = 0;
	String stockcheck = "";
	static double total = 0;
	static double count = 0;

	/**
	 * ---------------------------Customer----------------------------------------------
	 */

	/**
	 * Mapping for when to save a customers order to the 'orders' dbtable
	 * repository.
	 * 
	 * @return String webpage extension user sees
	 */
	@RequestMapping(value = "/add")
	public String saveCustomer() {

		currentOrder.setStatus("AWAITING APPROVAL");
		currentOrder.setPaid("PAID");
		ordersrep.save(currentOrder);

		return "redirect:/ordercomplete/" + currentOrder.getOrderID();
	}

	/**
	 * Mapping for when home page loads and menu is outputted to user.
	 * 
	 * @param model webpage object to add new attributes to
	 * @return String webpage extension user sees after event
	 */
	@RequestMapping("/")
	public String viewHomePage(Model model) {

		Tables table = new Tables();
		model.addAttribute("tables", table);
		List<Menu> listMenu = menurep.findAll();
		List<Menu> listFood = new ArrayList<Menu>();
		List<Menu> listTequila = new ArrayList<Menu>();
		List<Menu> listCocktails = new ArrayList<Menu>();
		List<Menu> listHotdrinks = new ArrayList<Menu>();

		for (Menu m : listMenu) {

			switch (m.getType()) {
			case "Tequila":
				listTequila.add(m);
			case "Cocktail":
				listCocktails.add(m);
			case "HotDrink":
				listHotdrinks.add(m);
			default:
				listFood.add(m);
			}

		}

		model.addAttribute("listFood", listFood);
		model.addAttribute("listTequila", listTequila);
		model.addAttribute("listCocktails", listCocktails);
		model.addAttribute("listHotdrinks", listHotdrinks);
		model.addAttribute("listMenu", listMenu);
		neworder = new Orders();
		model.addAttribute("listOrder", neworder);

		return "index";
	}

	/**
	 * Mapping for when user wants add an item to their order basket.
	 * 
	 * @param dishid The dishid of the item the user wants to edit
	 * @return Model and View predetermined for the edit functionality
	 */
	@RequestMapping("/addOrder/{dishid}")
	public String saveDish(@PathVariable(name = "dishid") int dishid) {
		ModelAndView mav = new ModelAndView("customer");
		Menu menu = menurep.findById(dishid).get();
		// String store= menu.getName() + " " + menu.getPrice();
		currentorders.add(menu.getName());
		currentorderprices.add(menu.getPrice());
		neworder.setPrice(Double.parseDouble(menu.getPrice()));
		neworder.setDescription(Integer.toString(menu.getMenuID()) + ",");
		// neworder.setStatus("AWAITING APPROVAL");
		mav.addObject("menu", menu);
		return "redirect:/customer";
	}

	/**
	 * Mapping for when a user requests help, sending over a table object.
	 * 
	 * @param table Table where the user is requesting help.
	 * @return String to which user is redirected after submission.
	 */
	@RequestMapping(value = "/addCustomerHelp", method = RequestMethod.POST)
	public String saveCustomerHelp(@ModelAttribute("tables") Tables table) {
		tablesrep.save(table);
		return "redirect:/";

	}

	/**
	 * Mapping for when a user wants to see their order and its status.
	 * 
	 * @param Ordernum Object which has the number to which the user is requesting
	 *                 to see its status.
	 * @return String which contains the path to where the user will have the order
	 *         status displayed.
	 */
	@RequestMapping(value = "/orderTrack", method = RequestMethod.POST)
	public String customerOrderTracking(@ModelAttribute("tables") Tables Ordernum) {
		return "redirect:/ordercomplete/" + Ordernum.getTablenum();

	}

	/**
	 * Redirection to the customer payment page after he has placed the order.
	 * 
	 * @param order The order to which the payment is associated to.
	 * @return Redirection of the customerPayment page.
	 */
	@RequestMapping("/orderPayment")
	public ModelAndView startPayment(@ModelAttribute("orders") Orders order) {
		ModelAndView mav = new ModelAndView("customerPayment");
		mav.addObject("order", order);

		currentOrder = new Orders();

		List<Menu> listMenu = menurep.findAll();
		// used to tally menu item orders
		if (countstored == false) {
			for (Menu menuitem : listMenu) {
				ordercount.put(menuitem.getName(), 0);
			}
		}

		double ordertotal = 0;
		String[] orderitems = order.getDescription().split(" ");
		for (String orderx : orderitems) {
			String quantorder[] = orderx.split(":");
			Menu menu = menurep.findById(Integer.parseInt(quantorder[1])).get();
			ordertotal += Integer.parseInt(quantorder[0]) * Double.parseDouble(menu.getPrice());
		}

		order.setOrdertime(getTime());
		double roundedtotal = Math.round(ordertotal * 100.0) / 100.0;
		order.setPrice(roundedtotal);
		order.setWaiterID(0);
		currentOrder = order;

		return mav;
	}

	/**
	 * Mapping for when user wants to confirm their current order basket.
	 * 
	 * @return Model and View predetermined for the edit functionality
	 */
	@RequestMapping("/confirmOrder")
	public ModelAndView sendOrder() {
		ModelAndView mav = new ModelAndView("ordercomplete");
		BigDecimal bd = new BigDecimal(neworder.getPrice()).setScale(2, RoundingMode.HALF_DOWN);
		neworder.setPrice(-neworder.getPrice());
		neworder.setStatus("Received");
		neworder.setPrice(bd.doubleValue());
		neworder.setOrdertime(getTime());
		ordersrep.save(neworder);
		List<Orders> listorders = ordersrep.findAll();
		int newid = 0;
		for (Orders list : listorders) {
			if (list.getOrderID() >= newid) {
				newid = list.getOrderID();
			}
		}
		neworder.setOrderID(newid);
		mav.addObject("orders", ordersrep);

		return mav;
	}

	/**
	 * ---------------------------Kitchen-----------------------------------------------
	 */

	/**
	 * Mapping for when kitchen page loads and orders are outputted to kitchen
	 * staff.
	 * 
	 * @param model webpage object to add new attributes to
	 * @return String webpage extension user sees after event
	 */

	@RequestMapping("/kitchen")
	public String viewKitchenPage(Model model) {
		List<Kitchen> listOrders = kitchenrep.findAll();
		model.addAttribute("listOrders", listOrders);

		return "kitchen";
	}

	/**
	 * Mapping for when kitchen wants to change status of an order.
	 * 
	 * @param kitchenID identifier for the order that the kitchen has accepted
	 * @return String webpage extension user sees after event
	 */
	@RequestMapping("/changeStatus/{kitchenID}")
	public String changeStatus(@PathVariable(name = "kitchenID") int kitchenID) {
		Kitchen kitchen = kitchenrep.findById(kitchenID).get();
		Orders order = ordersrep.findById(kitchen.getOrderID()).get();
		if (kitchen.getStatus().equals("READY") || kitchen.getStatus().equals("Delivered")) {

		} else {
			kitchen.setStatus("READY");
			order.setStatus("READY");
			ordersrep.save(order);
			kitchenrep.save(kitchen);
		}

		return "redirect:/kitchen";
	}

	/**
	 * ---------------------------Staff-------------------------------------------------
	 */

	/**
	 * Mapping for when home page loads and menu is outputted to user.
	 * 
	 * @param model webpage object to add new attributes to
	 * @return String webpage extension user sees after event
	 */
	@RequestMapping("/staffmenu")
	public String viewStaffMenu(Model model) {

		List<Staff> listStaff = staffrep.findAll();
		model.addAttribute("listStaff", listStaff);

		return "staffmenu";
	}

	/**
	 * Mapping for the loading of the staff "login" page where the waiters can
	 * navigate to their individual tables or other staff navigate to their
	 * respected page.
	 * 
	 * @param staffID The ID of the staff member that will be "login on"
	 * @return Model and View predetermined for the staff view functionality
	 */
	@RequestMapping("/stafflogin/{staffID}")
	public ModelAndView viewStaffLogin(@PathVariable(name = "staffID") int staffID) {
		Staff staffon = staffrep.findById(staffID).get();
		staffon.setOnduty("ON");
		staffrep.save(staffon);
		List<Orders> listOrder = ordersrep.findAll();
		List<Orders> waitersorders = new ArrayList<Orders>();
		List<Orders> unnassigned = new ArrayList<Orders>();
		List<Tables> helptables = tablesrep.findAll();

		for (Orders o : listOrder) {
			if (o.getWaiterID() == staffID) {
				waitersorders.add(o);
			}
			if (o.getWaiterID() == 0) {
				unnassigned.add(o);
			}

		}
		List<Tables> listTable = tablesrep.findAll();
		ModelAndView mav = new ModelAndView("staffOrders");
		Staff staff = staffrep.findById(staffID).get();
		mav.addObject("waitersorders", waitersorders);
		mav.addObject("listTable", listTable);
		mav.addObject("listOrder", listOrder);
		mav.addObject("unnassigned", unnassigned);
		mav.addObject("staff", staff);
		mav.addObject("helptables", helptables);

		return mav;
	}

	/**
	 * Mapping for when manageroverview loads and is outputted to user.
	 * 
	 * @param model webpage object to add new attributes to
	 * @return String webpage extension user sees after event
	 */
	@RequestMapping("/manageroverview")
	public String viewManagerOverview(Model model) {
		List<Menu> listMenu = menurep.findAll();
		List<Orders> listOrders = ordersrep.findAll();
		List<Stock> listStock = stockrep.findAll();
		List<Kitchen> listKitchen = kitchenrep.findAll();
		List<Staff> listStaff = staffrep.findAll();
		List<Sales> listSales = salesrep.findAll();
		HashMap<Integer, Double> staffsales = new HashMap<Integer, Double>();
		DecimalFormat df = new DecimalFormat("#.00");

		model.addAttribute("listSales", listSales);
		for (Staff staff : listStaff) {
			for (Sales s : listSales) {
				if (s.getStaffID() == staff.getStaffID()) {
					if (staffsales.get(s.getStaffID()) == null) {
						staffsales.put(s.getStaffID(), 0.0);
					} else {
						double currentsales = staffsales.get(s.getStaffID());
						BigDecimal bd = BigDecimal.valueOf((s.getValue() + currentsales));
						bd = bd.setScale(2, RoundingMode.HALF_UP);
						staffsales.put(staff.getStaffID(), bd.doubleValue());
					}
				}
			}
		}
		model.addAttribute("staffsales", staffsales);

		model.addAttribute("listStaff", listStaff);
		model.addAttribute("listOrders", listOrders);
		model.addAttribute("listKitchen", listKitchen);
		model.addAttribute("listStock", listStock);
		model.addAttribute("listOrdersStockReq", getCurrentStockReq());

		String bestseller = "";
		long diff = 0;

		if (countstored == false) {
			for (Menu menuitem : listMenu) {
				ordercount.put(menuitem.getName(), 0);
				bestseller = "-";
			}
		} else {
			bestseller = ordercount.entrySet().stream()
					.max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1).get().getKey();

		}

		for (Orders o : listOrders) {
			if (o.getStatus().equals("Delivered")) {
				diff = Math.abs(o.getDeliverytime().getTime() - (o.getOrdertime().getTime()));
				long minutes = (diff / (60 * 1000)) % 60;
				long seconds = (diff / 1000) % 60;
				timetaken.put(o, Long.toString(minutes) + ":" + Long.toString(seconds));
			}

		}

		DecimalFormat f = new DecimalFormat("##.00");
		model.addAttribute("timetaken", timetaken);
		model.addAttribute("bestseller", bestseller);
		model.addAttribute("listMenu", listMenu);
		model.addAttribute("ordercount", ordercount);
		model.addAttribute("revenue", f.format(total));
		model.addAttribute("numbercustomers", count);
		model.addAttribute("averagespend", f.format(total / count));

		return "manageroverview";
	}

	/**
	 * View mapping that allows the view of menu items by ID.
	 * 
	 * @param menuID ID of the item to be added to the view object
	 * @return Model and View predetermined for the display of the manager prices
	 */
	@RequestMapping("/managerprices/{menuID}")
	public ModelAndView viewManagerPrices(@PathVariable(name = "menuID") int menuID) {
		ModelAndView mav = new ModelAndView("managerprices");
		Menu managermenu = menurep.findById(menuID).get();
		mav.addObject("menu", managermenu);

		return mav;
	}

	/**
	 * Mapping for when to save a new menu to the repository.
	 * 
	 * @param menu       Attribute already in model previously posted to back end
	 * @param file       Multipart file that will be used as image for adding items
	 *                   to menu
	 * @param attributes The path that the file will be directed to
	 * @return String webpage extension user sees
	 */
	@RequestMapping(value = "/managersave", method = RequestMethod.POST)
	public String saveManagerProduct(@ModelAttribute("menu") Menu menu, MultipartFile file,
			RedirectAttributes attributes) {

		Menu newitem = menurep.save(menu);

		// check if file is empty
		if (file == null || file.isEmpty()) {
			attributes.addFlashAttribute("message", "Please select a file to upload.");
			return "redirect:/managernew";
		}
		


		// save the file on the local file system
		try {
			Path path = Paths.get(UPLOAD_DIR + newitem.getMenuID() + ".jpg");
			Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return "redirect:/managermenu";
	}


	/**
	 * Mapping for when a manager would like to delete an existing menu item.
	 * 
	 * @param menuID The menuID of the item the user wants to delete
	 * @return String webpage extension user sees after event
	 */
	@RequestMapping("/managerdelete/{menuID}")
	public String deleteManagerProduct(@PathVariable(name = "menuID") int menuID) {
		menurep.deleteById(menuID);
		return "redirect:/managermenu";
	}

	/**
	 * Mapping for the manager menu page to load and be displayed to the manager.
	 * 
	 * @param model webpage object to add new attributes to
	 * @return String webpage extension user sees after event
	 */
	@RequestMapping("/managermenu")
	public String viewManagerMenuPage(Model model) {
		List<Menu> listMenu = menurep.findAll();
		model.addAttribute("listMenu", listMenu);

		return "managermenu";
	}

	/**
	 * Mapping for the manager product page where all the menu items are diplayed.
	 * 
	 * @param model webpage object to add new attributes to
	 * @return String webpage extension user sees after event
	 */
	@RequestMapping("/managernew")
	public String showNewManagerProductPage(Model model) {
		Menu menu = new Menu();
		model.addAttribute("menu", menu);

		return "managercreate";
	}

	/**
	 * Mapping for when to save a new menu to the repository.
	 * 
	 * @param menu Attribute already in model previously posted to back end
	 * @return String webpage extension user sees
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveProduct(@ModelAttribute("menu") Menu menu) {
		menurep.save(menu);

		return "redirect:/managermenu";
	}

	/**
	 * Mapping for when user wants to edit an existing menu item.
	 * 
	 * @param menuID The menuID of the item the user wants to edit
	 * @return Model and View predetermined for the edit functionality
	 */
	@RequestMapping("/edit/{menuID}")
	public ModelAndView showEditProductPage(@PathVariable(name = "menuID") int menuID) {
		ModelAndView mav = new ModelAndView("edit");
		Menu menu = menurep.findById(menuID).get();
		mav.addObject("menu", menu);

		return mav;
	}

	/**
	 * Mapping for when user wants to delete an existing menu item.
	 * 
	 * @param menuID The menuID of the item the user wants to delete
	 * @return String webpage extension user sees after event
	 */
	@RequestMapping("/delete/{menuID}")
	public String deleteProduct(@PathVariable(name = "menuID") int menuID) {
		menurep.deleteById(menuID);
		return "redirect:/staff";
	}

	/**
	 * Mapping for when user wants to edit an existing menu item.
	 * 
	 * @param orderID The orderID of the item the user wants to edit
	 * @return Model and View predetermined for the edit functionality
	 */
	@RequestMapping("/ordercomplete/{orderID}")
	public ModelAndView displayOrderComplete(@PathVariable(name = "orderID") int orderID) {
		ModelAndView mav = new ModelAndView("ordercomplete");
		Orders order = ordersrep.findById(orderID).get();
		mav.addObject("order", order);

		return mav;
	}

	/**
	 * Method to give the requested stock items from orders
	 * 
	 * @return HashMap of Orders and the items required for the orders
	 */
	private HashMap<Orders, HashMap<String, Integer>> getCurrentStockReq() {
		stockrequirement = new HashMap<Orders, HashMap<String, Integer>>();
		List<Orders> listOrder = ordersrep.findAll();
		for (Orders o : listOrder) {
			stockrequirement.put(o, getOrderStockReq(o));
		}
		return stockrequirement;
	}

	/**
	 * Mapping for when waiter wants to confirm an order and send to kitchen.
	 * 
	 * @param orderID identifier for the order that the waiter has confirmed
	 * @param staffID identifier for the staff member confirming the order
	 * @return String webpage extension user sees after event
	 */
	@RequestMapping("/confirm/{orderID}/{staffID}")
	public String confirmOrder(@PathVariable("orderID") int orderID, @PathVariable("staffID") int staffID) {

		List<Stock> listStock = stockrep.findAll();
		List<Orders> listOrder = ordersrep.findAll();

		for (Orders o : listOrder) {
			if (o.getOrderID() == orderID) {
				for (String st : getOrderStockReq(o).keySet()) {
					for (Stock sto : listStock) {
						if (sto.getItemname().equals(st)) {
							sto.setQuantity(sto.getQuantity() - getOrderStockReq(o).get(st));
							stockrep.save(sto);
						} else
							continue;
					}
				}
				countstored = true;
				String quant[] = o.getDescription().split(" ");
				for (String qu : quant) {
					String[] quantorder = qu.split(":");
					Menu menu = menurep.findById(Integer.parseInt(quantorder[1])).get();
					// count number of menu items ordered
					int oc = ordercount.get(menu.getName());
					ordercount.put(menu.getName(), oc + Integer.parseInt(quantorder[0]));
					total += Integer.parseInt(quantorder[0]) * Double.parseDouble(menu.getPrice());
					count++;
				}

				if (ordersrep.findById(orderID).get().getStatus().equals("READY")
						|| ordersrep.findById(orderID).get().getStatus().equals("Delivered")) {
				} else {
					o.setStatus("SENT TO KITCHEN");
					o.setWaiterID(staffID);
					ordersrep.save(o);
				}
			} else {
				continue;
			}
		}

		List<Orders> allorders = ordersrep.findAll();
		if (ordersrep.findById(orderID).get().getStatus().equals("READY")
				|| ordersrep.findById(orderID).get().getStatus().equals("Delivered")) {
		} else if (ordersrep.findById(orderID).get().getStatus().equals("RECEIVED")) {
			for (Orders item : allorders) {
				if (item.getOrderID() == orderID) {
					item.setStatus("PREPARING");
					Kitchen kitchenItem = new Kitchen(orderID, item.getLocation(), item.getDescription(),
							item.getStatus(), getTime());
					kitchenrep.save(kitchenItem);
				}
			}
		} else {
			Orders ord = ordersrep.findById(orderID).get();
			Kitchen kitchenItem = new Kitchen(orderID, ord.getLocation(), ord.getDescription(), ord.getStatus(),
					getTime());

			kitchenrep.save(kitchenItem);
		}

		return "redirect:/stafflogin/" + staffID;
	}

	/**
	 * Mapping for when waiter wants to reject an order.
	 * 
	 * @param orderID identifier for the order that the waiter has rejected
	 * @param staffID identifier for the staff member confirming the order
	 * @return String webpage extension user sees after event
	 */
	@RequestMapping("/reject/{orderID}/{staffID}")
	public String rejectOrder(@PathVariable("orderID") int orderID, @PathVariable("staffID") int staffID) {
		ordersrep.deleteById(orderID);
		if (staffID == 0) {
			return "redirect:/";
		} else {
			return "redirect:/stafflogin/" + staffID;
		}
	}

	/**
	 * Mapping for waiter to delete an request for the tables repository
	 * 
	 * @param RequestID The ID of the request that is to be deleted.
	 * @param staffID   ID of the staff member that will be changing the status of
	 *                  the order
	 * @return String webpage extension that the user will be redirected to.
	 */
	@RequestMapping("/complete/{RequestID}/{staffID}")
	public String complete(@PathVariable(name = "RequestID") int RequestID, @PathVariable("staffID") int staffID) {
		tablesrep.deleteById(RequestID);
		return "redirect:/stafflogin/" + staffID;
	}

	/**
	 * Mapping for the status of delivery of the items on the staff that served and
	 * the item itself
	 * 
	 * @param orderID ID of the order that will have the status changed
	 * @param staffID ID of the staff member that will be changing the status of the
	 *                order
	 * @return String address where the user will be redirected to
	 */
	@RequestMapping("/delivered/{orderID}/{staffID}")
	public String statusDelivered(@PathVariable("orderID") int orderID, @PathVariable("staffID") int staffID) {
		List<Kitchen> listKitchen = kitchenrep.findAll();
		Kitchen kitchen = new Kitchen();
		for (Kitchen k : listKitchen) {

			if (k.getOrderID() == orderID) {
				kitchen = k;
			}
		}

		Orders order = ordersrep.findById(orderID).get();
		if (kitchen.getStatus().equals("READY")) {
			kitchen.setStatus("Delivered");
			order.setStatus("Delivered");
			order.setDeliverytime(getTime());
			ordersrep.save(order);
			kitchenrep.save(kitchen);
			Sales newsale = new Sales(getTimeStamp(), order.getDescription(), order.getWaiterID(), order.getPrice());
			salesrep.save(newsale);
		}
		return "redirect:/stafflogin/" + staffID;
	}

	/**
	 * --------------------Calculation Functions------------------------------------
	 */

	/**
	 * Obtains current time.
	 * 
	 * @return Time current time
	 */
	public Time getTime() {
		Date now = new java.util.Date();
		Time current = new java.sql.Time(now.getTime());
		return current;
	}

	/**
	 * Obtains current time Stamp.
	 * 
	 * @return Time current time stamp
	 */
	public Timestamp getTimeStamp() {
		Date now = new java.util.Date();
		Timestamp current = new java.sql.Timestamp(now.getTime());
		return current;
	}

	/**
	 * Returns Hashmap of orders recipe requirements.
	 * 
	 * @param order Orders object
	 * @return HashMap containing item name and quantity required
	 */
	private HashMap<String, Integer> getOrderStockReq(Orders order) {

		recipeitems = new HashMap<String, Integer>();
		orderitems = new HashMap<>(recipeitems);
		String[] description = order.getDescription().split(" ");
		// for each item in described in description
		for (String item : description) {
			// separate quantity from item
			description = item.split(":");
			// get menuitem recipe
			for (int x = 0; x <= Integer.parseInt(description[0]) - 1; x++) {
				recipeitems = setRecipeItems(menurep.findById(Integer.parseInt(description[1])).get());
				recipeitems.forEach((key, value) -> orderitems.merge(key, value, (v1, v2) -> v1 + v2));

			}
		}
		return orderitems;
	}

	/**
	 * Sets HashMap with recipe item names and quantities, referencing menu item.
	 * 
	 * @param menuitem An instance of Menu, an item for which a recipe is required
	 * @return HashMap recipe in the form of an item name and quantity required
	 */
	private HashMap<String, Integer> setRecipeItems(Menu menuitem) {
		recipeitems = new HashMap<String, Integer>();
		String[] array = menuitem.getIngID().split(" ");
		// for each ingredient requirement list
		for (String ingredient : array) {
			// split list into item and quantity required
			String[] ing = ingredient.split(":");
			// put in recipe items hash
			recipeitems.put(ing[0], Integer.parseInt(ing[1]));
		}
		return recipeitems;
	}

}
