package stockmanager;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Stock object to store the dishes on order. 
 * @author Sean
 */
@Entity
public class Stock {
	private String itemname;
	private int quantity;
	
	/**
	 * Empty Constructor for the Stock object.
	 */
	public Stock() {
	}

	/**
	 * Constructor of stock object.
	 * @param itemname id of the ingredient 
	 * @param quantity amount in store of the ingredient
	 */
	public Stock(String itemname, int quantity) {
		super();
		this.itemname = itemname;
		this.quantity = quantity;
	
	}

	/**
	 * Getter for ID of order.
	 * @return Integer id of order
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public String getItemname() {
		return itemname;
	}

	/**
	 * Setter for ID of ingredient.
	 * @param itemname String id of ingredient.
	 */
	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	/**
	 * Getter for the quantity of ingredient.
	 * @return quantity String ID of ingredient
	 */
	public int getQuantity() {
		return quantity;
	}

    /**
     * Setter for the quantity of order.
     * @param quantity of order
     */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

  
	}
	
