package stockmanager;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import kitchenmanager.Kitchen;


/**
 * Creates repository object to interface with JPArepo that stores the kitchen items.
 * @author Sean, Seb
 */
public interface StockRepository extends JpaRepository<Stock, Integer> {


}


