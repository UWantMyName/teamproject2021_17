package menumanager;

import org.springframework.data.jpa.repository.JpaRepository;

import menumanager.Menu;

/**
 * Creates repository object to interface with JPArepo that stores the menu.
 * @author Sean, Shreyas
 */
public interface MenuRepository extends JpaRepository<Menu, Integer> {

}
