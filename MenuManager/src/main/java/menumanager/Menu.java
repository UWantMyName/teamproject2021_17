package menumanager;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Menu object to store the various dishes offered. 
 * @author Sean, Shreyas
 */
@Entity
public class Menu {
	private int menuID;
	private String name;
	private String description;
	private String type;

	private String allergies;
	private String calories;
	private String price;
	private String ingID;

	/**
	 * Empty Constructor for the Menu object.
	 */
	public Menu() {
	}

	/**
	 * Constructor of menu object.
	 * @param menuID id of the item 
	 * @param name name of the item
	 * @param description brief explanation of the item
	 * @param type the type of the item (vegan, veg etc.)
	 * @param price price in GBP of the item
	 */
	protected Menu(int menuID, String name, String description, String type, String price, String ingID) {
		super();
		this.menuID = menuID;
		this.name = name;
		this.description = description;
		this.type = type;
		this.price = price;
		this.ingID = ingID;
	}

	/**
	 * Getter for ID of item.
	 * @return Integer id of item
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getMenuID() {
		return menuID;
	}

	/**
	 * Setter for ID of item.
	 * @param menuID id of item.
	 */
	public void setMenuID(int menuID) {
		this.menuID = menuID;
	}

	/**
	 * Getter for the name of item.
	 * @return String name of item
	 */
	public String getName() {
		return name;
	}

    /**
     * Setter for the name of item.
     * @param name of item
     */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Getter for the description of item.
	 * @return String description of item
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Setter for the description of item.
	 * @param description of item
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Getter for the type of item.
	 * @return String type associated with item
	 */
	public String getType() {
		return type;
	}

	/**
	 * Setter for the type of item.
	 * @param type type associated with item
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Getter for the type of item.
	 * @return String type associated with item
	 */
	public String getCalories() {
		return calories;
	}

	/**
	 * Setter for the type of item.
	 * @param calories number of calories to be set to
	 */
	public void setCalories(String calories) {
		this.calories = calories;
	}
	
	/**
	 * Getter for the type of item.
	 * @return String type associated with item
	 */
	public String getAllergies() {
		return allergies;
	}

	/**
	 * Setter for the type of item.
	 * @param allergies string that contains the allergies will be set to the item
	 */
	public void setAllergies(String allergies) {
		this.allergies = allergies;
	}

	/**
	 * Getter for the price of item.
	 * @return String price of item
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * Getter for the type of item.
	 * @param price price of item
	 */
	public void setPrice(String price) {
		this.price = price;
	}
	
	/**
	 * Getter for the ingredients of item.
	 * @return String ingredients of item
	 */
	public String getIngID() {
		return ingID;
	}

	/**
	 * Getter for the indID of item.
	 * @param ingID the ID of the ingrediant
	 */
	public void setIngID(String ingID) {
		this.ingID = ingID;
	}

}
