package salesmanager;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Object that contains details of a sale and the waiter who made the sale.
 * 
 * @author Sean, Valerie, Shreyas, Stefan, Molly
 */
@Entity
public class Sales {
	private int saleID;
	private Timestamp orderdate;
	private String description;
	private int staffID;
	private double value;


	/**
	 * Empty Constructor for the Sales object.
	 */
	public Sales() {
	}

	/**
	 * Constructor of sales object.
	 * 
	 * @param orderdate   date the order was placed
	 * @param description brief explanation of the sale
	 * @param staffID     id of the staff
	 * @param value       total value of the sale
	 */
	public Sales(Timestamp orderdate, String description, int staffID, double value) {
		super();
		this.orderdate = orderdate;
		this.description = description;
		this.staffID = staffID;
		this.value = value;

	}

	/**
	 * Returns sale id.
	 * 
	 * @return Integer id of the sale
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getSaleID() {
		return saleID;
	}

	/**
	 * Sets sale ID.
	 * 
	 * @param saleID is the ID of the sale
	 */
	public void setSaleID(int saleID) {
		this.saleID = saleID;
	}

	/**
	 * Returns the date and time of the order.
	 * 
	 * @return order date and time of the order
	 */
	public Timestamp getOrderdate() {
		return orderdate;
	}

	/**
	 * Sets order date and time.
	 * 
	 * @param orderdate date and time of order placed
	 */
	public void setOrderdate(Timestamp orderdate) {
		this.orderdate = orderdate;
	}

	/**
	 * Returns description of the sale made.
	 * 
	 * @return String description of sale
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets description of the sale.
	 * 
	 * @param description of sale
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets staff id.
	 * 
	 * @param staffID integer id of the staff
	 */
	public void setStaffID(int staffID) {
		this.staffID = staffID;
	}
	
	/**
	 * Returns staff id.
	 * 
	 * @return int staffID of staff id
	 */
	public int getStaffID() {
		return staffID;
	}

	/**
	 * Returns total price of the sale.
	 * 
	 * @return value of sale
	 */
	public double getValue() {
		return value;
	}

	/**
	 * Sets the total price of the sale.
	 * 
	 * @param value of sale
	 */
	public void setValue(double value) {
		this.value = value;

	}

}