package ordersmanager;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Object that contains the orders that the user has made.
 * 
 * @author Sean, Valerie, Shreyas, Stefan, Molly
 */

@Entity
public class Orders {
	private int orderID;
	private int location = 0;
	private String description = "";
	private String name = "";
	private double price = 0;
	private Time ordertime = null;
	private int waiterID = 0;
	private String status = "";
	private Time deliverytime = null;
	Date now = new java.util.Date();
	Time current = new java.sql.Time(now.getTime());
	private String paid = "NOT PAID";

	/**
	 * Empty Constructor for the Orders object
	 */
	public Orders() {
	}

	/**Constructor with the table number as its parameter.
	 * @param table_num int of the table that location will be set to
	 */
	public Orders(int table_num) {
		this.location = table_num;
	}

	/**
	 * Constructor of order object.
	 * 
	 * @param orderID     ID of the order
	 * @param location    number of the table
	 * @param description a string of the dishes ordered
	 * @param name        name of customer
	 * @param status      current status associated with that order
	 * @param price       total price of the dishes ordered
	 */
	public Orders(int orderID, int location, String description, String name, String status, double price) {
		super();
		this.orderID = orderID;
		this.location = location;
		this.description = description;
		this.name = name;
		this.status = status;
		this.price = price;

	}

	/**
	 * Gets the order id.
	 * 
	 * @return Integer id of the order
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getOrderID() {
		return orderID;
	}

	/**
	 * Sets order ID.
	 * 
	 * @param orderID id of order.
	 */
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	/**
	 * Gets the location.
	 * 
	 * @return the location of order
	 */
	public int getLocation() {
		return location;
	}

	/**
	 * 
	 * Sets location.
	 * 
	 * @param location of order
	 */
	public void setLocation(int location) {
		this.location = location;
	}

	/**
	 * Setter for description of order.
	 * 
	 * @param description of items in the order
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Getter for the description of order.
	 * 
	 * @return String description of order
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Gets the name of the customer whose order it is.
	 * 
	 * @return String name of order
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the customer whose order it is.
	 * 
	 * @param name of order
	 */
	public void setName(String name) {
		this.name = name;

	}

	/**
	 * Gets the price of the order.
	 * 
	 * @return String price of order
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * Sets the price of the order.
	 * 
	 * @param price price of order
	 */
	public void setPrice(double price) {

		this.price = price;

	}

	/**
	 * Setter for the paid variable.
	 * 
	 * @param paid Variable specifying whether the order has been paid for or not.
	 */
	public void setPaid(String paid) {
		this.paid = paid;
	}

	/**
	 * Getter for the paid variable.
	 * 
	 * @return Whether the order has been paid for or not.
	 */
	public String getPaid() {
		return this.paid;
	}

	/**
	 * Getter for the order time of order.
	 * 
	 * @return TimeStamp associated with order
	 */
	public Time getOrdertime() {
		return ordertime;
	}

	/**
	 * Setter for the order time of order.
	 * 
	 * @param ordertime TimeStamp associated with item
	 */
	public void setOrdertime(Time ordertime) {

		this.ordertime = ordertime;
	}

	/**
	 * Getter for the delivery time of order.
	 * 
	 * @return TimeStamp associated with order
	 */
	public Time getDeliverytime() {
		return deliverytime;
	}

	/**
	 * Setter for the delivery time of order.
	 * 
	 * @param deliverytime TimeStamp associated with item
	 */
	public void setDeliverytime(Time deliverytime) {

		this.deliverytime = deliverytime;
	}

	/**
	 * Getter for the status of order.
	 * 
	 * @return status associated with order
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Setter for the status of order.
	 * 
	 * @param status status associated with item
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Rounds double to two decimal places
	 * 
	 * @param value  added to current value in order to produce new total
	 * @param places the decimal places number is to be rounded to
	 * @return the rounded number
	 */
	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = BigDecimal.valueOf(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	/**
	 * Setter for the status of order.
	 * 
	 * @param waiterID the ID of waiter to set to the object
	 */
	public void setWaiterID(int waiterID) {
		this.waiterID = waiterID;
	}

	/**
	 * Getter for the status of order.
	 * 
	 * @return status associated with order
	 */
	public int getWaiterID() {
		return waiterID;
	}
}