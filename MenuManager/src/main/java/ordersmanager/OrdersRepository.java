package ordersmanager;

import org.springframework.data.jpa.repository.JpaRepository;

import ordersmanager.Orders;

/**
 * Creates repository object to interface with JPArepo that stores the users Orders.
 * @author Shreyas
 */
public interface OrdersRepository extends JpaRepository<Orders, Integer> {
}