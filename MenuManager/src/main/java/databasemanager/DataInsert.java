package databasemanager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * Class for Inserting the required data into the mysql database.
 * 
 * @author seb
 *
 */
public class DataInsert {

	/**
	 * This will insert data into the database by executing batches of data that is
	 * inserted from the file, by repeatedly running it until it has gone through
	 * the whole file and imported the data into the DB.
	 * 
	 * @param connection   The link that will allow communication with the existing
	 *                     database.
	 * @param file         The path to the file that has all the data for the
	 *                     database.
	 * @param composedLine The command to be executed
	 */
	public DataInsert(Connection connection, String file, String composedLine) {
		try {
			BufferedReader br = null;
			String sCurrentLine;
			int count = 0;
			br = new BufferedReader(new FileReader(file));
			PreparedStatement statement = connection.prepareStatement(composedLine);

			while ((sCurrentLine = br.readLine()) != null) {

				String[] splitLine = sCurrentLine.split(",");

				for (int i = 0; i < splitLine.length; i++) {
					statement.setString(i + 1, splitLine[i]);
				}

				statement.addBatch();
				count++;
				if (count % 100 == 0)
					statement.executeBatch();
			}

			statement.executeBatch();
			br.close();

		} catch (Exception e) {

			e.printStackTrace();
		}
	}
}
