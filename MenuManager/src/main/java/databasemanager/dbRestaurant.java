package databasemanager;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Database controls to initiate the structure of the database that will be used
 * in the system, including the schema and table structure
 * 
 * @author Sean, Valerie, Shreyas, Stefan
 *
 */
public class dbRestaurant {

	/**
	 * Main that will run on the execution of dbrestaurant connecting to the
	 * database, creating required tables with specified fields and filling them
	 * with data where required/specified.
	 * 
	 * @param args ---
	 * @throws SQLException exception when the sql commands get incorrect input.
	 */
	public static void main(String[] args) throws SQLException {

		// Login Credentials for connecting to the database.
		String user = "root";
		String password = "student";
		// THe URL for the database.
		//
		// CHANGE IF THE NAME DOESNT MATCH YOUR DATABASE's
		// String database = "jdbc:mysql://localhost:3306/restaurante"; //- for the rest
		// of the team
		String database = "jdbc:mysql://localhost:3306/restaurante";
																																										
																																										
																																										
																																										

		Connection connection = connectToDatabase(user, password, database);
		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
			return;
		}

		DatabaseMetaData dbm = connection.getMetaData();

		String tablenames[] = { "menu", "orders", "kitchen", "tables", "stock", "staff" };

		for (String table : tablenames) {
			ResultSet rs = dbm.getTables(null, null, table, null);
			if (rs.next()) {
				dropTable(connection, table);
			}
		}

		createTable(connection,
				"menu(menuID SERIAL, name varchar(45) NOT NULL, description varchar(120) NULL, price varchar(20) NOT NULL, allergies varchar(100), calories varchar(100), type varchar(80) NOT NULL, ingID varchar(200) NOT NULL, PRIMARY KEY (menuID));");
		new DataInsert(connection, "src//main//java//databasemanager//database.csv",
				"INSERT INTO menu(name,description,price,allergies,calories,type,ingID) " + "VALUES(?,?,?,?,?,?,?)");
		createTable(connection,
				"orders(orderID SERIAL PRIMARY KEY, description varchar(120), location int NOT NULL, name varchar(80) NOT NULL, price DOUBLE(6, 2),ordertime TIME, deliverytime TIME, status varchar(120) NOT NULL, waiterID int, paid varchar(10) NOT NULL);");
		createTable(connection,
				"kitchen(kitchenID SERIAL PRIMARY KEY,orderID int NOT NULL, tablenum int NOT NULL,  description varchar(120) NOT NULL, status varchar(120) NOT NULL, ordertime TIME);");
		createTable(connection, "tables(RequestID SERIAL NOT NULL PRIMARY KEY, tablenum int NOT NULL)");
		createTable(connection, "stock(itemname varchar(50) NOT NULL PRIMARY KEY, quantity int NOT NULL)");
		new DataInsert(connection, "src//main//java//databasemanager//kitchenstock.csv",
				"INSERT INTO stock(itemname,quantity) " + "VALUES(?,?)");
		createTable(connection,
				"staff(staffID SERIAL PRIMARY KEY NOT NULL, name varchar(120) NOT NULL, role varchar(120) NOT NULL, onduty varchar(10) NOT NULL)");
		new DataInsert(connection, "src//main//java//databasemanager//staff.csv",
				"INSERT INTO staff(name, role, onduty)" + "VALUES(?,?,?)");
		createTable(connection,
				"sales(saleID SERIAL PRIMARY KEY NOT NULL, orderdate TIMESTAMP, description varchar(120) NOT NULL, staffID int NOT NULL, value DOUBLE(6,2) NOT NULL)");
	}

	/**
	 * Connection method for the database that will run on MySQL where all the data
	 * will be stored at
	 * 
	 * @param username The Login credential for the username for the Database.
	 * @param password The Login credential for the Password.
	 * @param database The URL of the database that will be connected to.
	 * @return The established connection to the DataBase.
	 */
	public static Connection connectToDatabase(String username, String password, String database) {
		System.out.println("-------- JDBC Connection Testing ------------");
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(database, username, password);
		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
		}
		return connection;
	}

	/**
	 * This will use the already established connection to the database to execute
	 * the create table statement with the name and structure given.
	 * 
	 * @param connection       The link that will allow communication with the
	 *                         existing database.
	 * @param tableDescription The variables and their descriptor for the database
	 *                         to create a table.
	 */
	private static void createTable(Connection connection, String tableDescription) {
		Statement st = null;
		try {
			st = connection.createStatement();
			st.execute("CREATE TABLE IF NOT EXISTS " + tableDescription);
			st.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * This will remove the table from the database to prevent any conflicts from
	 * occurring during creation of new one.
	 * 
	 * @param connection The link that will allow communication with the existing
	 *                   database.
	 * @param table      Name of the table that will be removed from the database.
	 */
	private static void dropTable(Connection connection, String table) {
		Statement st = null;
		try {
			st = connection.createStatement();
			st.execute("SET FOREIGN_KEY_CHECKS = 0");
			st.execute("DROP TABLE IF EXISTS " + table);
			st.execute("SET FOREIGN_KEY_CHECKS = 1");
			st.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
