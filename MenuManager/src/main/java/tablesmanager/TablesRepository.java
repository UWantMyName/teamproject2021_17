package tablesmanager;

import org.springframework.data.jpa.repository.JpaRepository;

import tablesmanager.Tables;

/**
 * Creates repository object to interface with JPArepo that stores the tables items.
 * @author Seba
 *
 */
public interface TablesRepository extends JpaRepository<Tables, Integer> {

}
