package tablesmanager;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Table Object that stores the tables that request help from waiters.
 * 
 * @author Seba
 *
 */
@Entity
public class Tables {
	@Column(name = "RequestID")
	private int RequestID;
	@Column(name = "Tablenum")
	private int Tablenum;

	/**
	 * Empty Constructor for the Tables object
	 */
	public Tables() {

	}

	/**
	 * Constructor for the tables object.
	 * 
	 * @param RequestID Unique ID for help requests from clients.
	 * @param Tablenum  table number of client that requested help.
	 */
	public Tables(int RequestID, int Tablenum) {
		super();
		this.RequestID = RequestID;
		this.Tablenum = Tablenum;
//		this.time = helptime;
	}

	/**
	 * Getter for the ID of the request.
	 * 
	 * @return The ID of the request.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getRequestID() {
		return RequestID;
	}

	/**
	 * Setter for the Id of the request.
	 * 
	 * @param RequestID ID that the request will be set to.
	 */
	public void setRequestID(int RequestID) {
		this.RequestID = RequestID;
	}

	/**
	 * Getter for the table number.
	 * 
	 * @return Table number.
	 */
	public int getTablenum() {
		return this.Tablenum;
	}

	/**
	 * Setter for the table number.
	 * 
	 * @param Tablenum Integer number that the table will be set to.
	 */
	public void setTablenum(int Tablenum) {
		this.Tablenum = Tablenum;
	}

//	public Timestamp getRequesttime() {
//		return this.time;
//	}
//
//	public void setRequesttime(Timestamp a) {
//		this.time = a;
//	}
}
