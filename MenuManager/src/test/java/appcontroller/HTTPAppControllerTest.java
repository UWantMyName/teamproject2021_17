package appcontroller;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class HTTPAppControllerTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void intitalLoadingIndexPageTest() throws Exception {
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/",
				String.class)).contains("Mini Burritos");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/",
				String.class)).contains("Burritos");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/",
				String.class)).contains("Fajitas");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/",
				String.class)).contains("Soft Tacos");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/",
				String.class)).contains("Tacos");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/",
				String.class)).contains("Nachos");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/",
				String.class)).contains("Enchiladas");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/",
				String.class)).contains("Order");
	}
	
	@Test
	public void intitalManagerMenuTest() throws Exception {
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managermenu",
				String.class)).contains("Mini Burritos");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managermenu",
				String.class)).contains("Burritos");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managermenu",
				String.class)).contains("Fajitas");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managermenu",
				String.class)).contains("Soft Tacos");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managermenu",
				String.class)).contains("Tacos");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managermenu",
				String.class)).contains("Nachos");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managermenu",
				String.class)).contains("Enchiladas");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managermenu",
				String.class)).contains("Edit");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managermenu",
				String.class)).contains("Delete");
	}
	
	@Test
	public void intitalkitchenTest() throws Exception {
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/kitchen",
				String.class)).contains("OrderID");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/kitchen",
				String.class)).contains("TableNumber");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/kitchen",
				String.class)).contains("MenuItems");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/kitchen",
				String.class)).contains("Status");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/kitchen",
				String.class)).contains("OrderTime");
	}
	
	
	@Test
	public void intitalLoadingmanagerpricesPageTest() throws Exception {
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managerprices/1",
				String.class)).contains("menuID:");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managerprices/1",
				String.class)).contains("name:");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managerprices/1",
				String.class)).contains("type:");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managerprices/1",
				String.class)).contains("description:");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managerprices/1",
				String.class)).contains("price:");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managerprices/1",
				String.class)).contains("ingredients:");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managerprices/1",
				String.class)).contains("save");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managerprices/1",
				String.class)).contains("Basic cost:");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managerprices/1",
				String.class)).contains("Calculate");
	}
	
	@Test
	public void intitalLoadingmanagercreatePageTest() throws Exception {
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managernew",
				String.class)).contains("Name:");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managernew",
				String.class)).contains("Description:");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managernew",
				String.class)).contains("Type:");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managernew",
				String.class)).contains("Image:");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managernew",
				String.class)).contains("Price:");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managernew",
				String.class)).contains("Ingredients:");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managernew",
				String.class)).contains("Save");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managernew",
				String.class)).contains("Basic cost:");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/managernew",
				String.class)).contains("Calculate");
	}
	
	@Test
	public void intitalLoadingmanageroverviewPageTest() throws Exception {
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/manageroverview",
				String.class)).contains("Stock");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/manageroverview",
				String.class)).contains("Orders");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/manageroverview",
				String.class)).contains("Menu Demand");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/manageroverview",
				String.class)).contains("Waiter Sales");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/manageroverview",
				String.class)).contains("Daily Stats");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/manageroverview",
				String.class)).contains("Sales History");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/manageroverview",
				String.class)).contains("Menu");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/manageroverview",
				String.class)).contains("SaleID");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/manageroverview",
				String.class)).contains("Description");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/manageroverview",
				String.class)).contains("StaffMember");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/manageroverview",
				String.class)).contains("Value");
	}
	
	@Test
	public void intitalLoadingorderConfirmationPageTest() throws Exception {
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Incoming Orders");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Order Number");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Table Number");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Order Description");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Customer Name");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Price");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Order Time");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Status");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Reject");
	}
	
	@Test
	public void intitalLoadingstaffOrdersPageTest() throws Exception {
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Incoming Orders");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Order Number");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Table Number");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Order Description");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Customer Name");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Price");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Order Time");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Status");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Accept");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Reject");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/stafflogin/1",
				String.class)).contains("Delivered");
	}
	
	@Test
	//Only passes after an order has been made outside of the TDD then the TDD tests is run
	public void intitalLoadingOrderCompleteTest() throws Exception {
		//only works if theres been an order placed
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/ordercomplete/1",
				String.class)).contains("Thankyou!");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/ordercomplete/1",
				String.class)).contains("Your order has been place sucsessfully");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/ordercomplete/1",
				String.class)).contains("Order number:");
	}
	
	@Test
	public void staffLoginPageTest() throws Exception {
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/staffmenu",
				String.class)).contains("Molly Howard");
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/staffmenu",
				String.class)).contains("Manager");
	}

}
