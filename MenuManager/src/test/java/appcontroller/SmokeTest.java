package appcontroller;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Test class for the appcontroler that has the methods for the application.
 */
@SpringBootTest
public class SmokeTest {

	@Autowired
	private AppController controller;

	/**Test to see if the controller actually runs.
	 * @throws Exception thrown if there is an error when controller is run
	 */
	@Test
	public void loads() throws Exception {
		assertThat(controller).isNotNull();
	}

}
