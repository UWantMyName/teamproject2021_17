// package menumanager;
// 
// import static org.junit.jupiter.api.Assertions.assertEquals; 
// import java.util.List; 
// import org.junit.jupiter.api.BeforeEach; 
// import org.junit.jupiter.api.Test;
// 
// class MenuServiceTest {
// 
// MenuService service;
// 
// Menu menu;
// 
// @BeforeEach 
// void setup() { 
//   menu = new Menu(0, "Item 1", "First item", "Type", "15.99"); 
// }
// 
// @Test 
// void SaveGetItemtest() { 
//   service.save(menu); 
//   Menu gotMenu = service.get(0); 
//   assertEquals(gotMenu.getName(), "Item 1","Menu object stored and retrieved from repo using menu service depending on its Menu ID"); 
// }
// 
// @Test 
// void SaveGetListtest() { 
//   service.save(menu); 
//   service.save(menu);
//   service.save(menu); 
//   List<Menu> menuList = service.listAll();
//   assertEquals(menuList.size(), 1, "Check if records saved to repo can be retrieved as list using listAll if duplicates then duplicates are not saved"); 
// }
// 
// @Test 
// void SaveDeleteItemtest() { 
//   service.save(menu); 
//   Menu menu1 = new Menu(1, "Item 2", "item", "veg", "17.99"); 
//   service.save(menu1); 
//   List<Menu>menuList = service.listAll(); 
//   assertEquals(menuList.size(), 2, "Check if records saved to repo can be retrieved as list using listAll");
//   service.delete(1); 
//   List<Menu> menuList1 = service.listAll();
//   assertEquals(menuList1.size(), 1, "Check if records saved to repo can be retrieved as list using listAll after deletion"); 
//   } 
// }
// 