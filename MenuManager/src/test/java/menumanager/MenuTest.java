package menumanager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import menumanager.Menu;

/**
 * Unit tests for the Menu class. 
 * @author Shreyas
 */
class MenuTest {

  Menu menu;

  @BeforeEach
  void setup() {
    menu = new Menu(0, "Item 1", "First item", "Type", "15.99","2");
  }

  @Test
  void EmptyConstructtest() {
    Menu emptmenu = new Menu();
    assertEquals(null, emptmenu.getName(), "Test if a empty menu item can be created");
  }

  @Test
  void FilledConstructtest() {
    assertEquals(0, menu.getMenuID(), "Test id of item is constructed correctly");
    assertEquals("Item 1", menu.getName(), "Test name of item is constructed correctly");
    assertEquals("First item", menu.getDescription(),
        "Test description of item is constructed correctly");
    assertEquals("Type", menu.getType(), "Test type of item is constructed correctly");
    assertEquals("15.99", menu.getPrice(), "Test price of item is constructed correctly");
  }

  @Test
  void EditMenuItemtest() {
    assertEquals(0, menu.getMenuID(), "Test id of item is constructed correctly");
    assertEquals("Item 1", menu.getName(), "Test name of item is constructed correctly");
    assertEquals("First item", menu.getDescription(), "Test description of item is constructed correctly");
    assertEquals("Type", menu.getType(), "Test type of item is constructed correctly");
    assertEquals("15.99", menu.getPrice(), "Test price of item is constructed correctly");

    menu.setMenuID(2);
    menu.setName("Name Change");
    menu.setDescription("The changed description");
    menu.setType("Changed Type");
    menu.setPrice("20.00");

    assertEquals(2, menu.getMenuID(), "Test id of item is changed");
    assertEquals("Name Change", menu.getName(), "Test name of item is changed");
    assertEquals("The changed description", menu.getDescription(), "Test description of item is changed");
    assertEquals("Changed Type", menu.getType(), "Test type of item is changed");
    assertEquals("20.00", menu.getPrice(), "Test price of item is changed");
  }

}
