package ordersmanager;
//package ordermanager;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//import java.util.List;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
//import kitchenmanager.Kitchen;
//import kitchenmanager.KitchenRepository;
//import kitchenmanager.KitchenService;
//import ordersmanager.Orders;
//import ordersmanager.OrdersRepository;
//
//class OrdersServiceTest {
//
//  OrdersService service;
//
//  KitchenService kservice;
//
//  OrdersRepository ordersrepo;
//  
//  KitchenRepository kitchenrepo;
//  
//  Orders order;
//  
//  @BeforeEach
//  void setup() {
//    order = new Orders(1, 12, "3, 5, 7", "bob", "none", 127.50);
//  }
//
//  @Test
//  void ConfimTest() {
//    ordersrepo.save(order);
//    ordersrepo.save(order);
//    ordersrepo.save(order);
//    List<Orders> ordersList = service.listAll();
//    assertEquals(ordersList.size(), 1,
//        "Check if records saved to repo can be retrieved as list using listAll if duplicates then duplicates are not saved");
//  }
//
//  @Test
//  void SaveGetListtest() {
//    ordersrepo.save(order);
//    service.confirm(order.getOrderID());
//    List<Kitchen> kitchenList = kservice.listAll();
//    assertEquals(kitchenList.size(), 1,
//        "Check if only one order is confirmed and sent to the kitchenrepo");
//    assertEquals(kitchenList.get(0).getOrderID(), 1,
//        "Check if correct order is confirmed and sent and saved into the kitchenrepo");
//  }
//
//  @Test
//  void SaveDeleteItemtest() {
//    ordersrepo.save(order);
//    Orders order1 = new Orders(1, 43, "2, 4", "alice", "none", 27.50);
//    ordersrepo.save(order1);
//    List<Orders> orderList = service.listAll();
//    assertEquals(orderList.size(), 2,
//        "Check if records saved to repo can be retrieved as list using listAll");
//    service.delete(1);
//    List<Orders> orderList1 = service.listAll();
//    assertEquals(orderList1.size(), 1,
//        "Check if records saved to repo can be retrieved as list using listAll after deletion");
//  }
//
//}
