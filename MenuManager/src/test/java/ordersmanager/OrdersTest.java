package ordersmanager;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ordersmanager.Orders;
import stockmanager.StockRepository;

/**
 * Unit tests for the Orders class.
 * 
 * @author Shreyas
 *
 */
class OrdersTest {

	Orders order;
	StockRepository stocklist;

	@BeforeEach
	void setup() {
		order = new Orders(1, 12, "3, 5, 7", "bob", "none", 127.50);
	}

	@Test
	void EmptyConstructtest() {
		Orders emptyorder = new Orders();
		assertEquals("", emptyorder.getName(), "Test if a empty order item can be created");
	}

	@Test
	void FilledConstructtest() {
		assertEquals(1, order.getOrderID(), "Test id of order is constructed correctly");
		assertEquals("bob", order.getName(), "Test name of order is constructed correctly");
		assertEquals("3, 5, 7", order.getDescription(), "Test description of order is constructed correctly");
		assertEquals(12, order.getLocation(), "Test location of order is constructed correctly");
		assertEquals(127.50, order.getPrice(), "Test price of order is constructed correctly");
		assertEquals("none", order.getStatus(), "Test the status of the order is constructed correctly");
		
	}

	@Test
	void EditMenuItemtest() {
		assertEquals(1, order.getOrderID(), "Test id of order is constructed correctly");
		assertEquals("bob", order.getName(), "Test name of order is constructed correctly");
		assertEquals("3, 5, 7", order.getDescription(), "Test description of order is constructed correctly");
		assertEquals(12, order.getLocation(), "Test location of order is constructed correctly");
		assertEquals(127.50, order.getPrice(), "Test price of order is constructed correctly");

		order.setOrderID(5);
		order.setName("alice");
		order.setDescription("The changed description");
		order.setLocation(43);
		order.setPrice(20.00);
		order.setStatus("10:24");
	    

		assertEquals(5, order.getOrderID(), "Test id of order is changed");
		assertEquals("alice", order.getName(), "Test name of order is changed");
		assertEquals("The changed description", order.getDescription(), "Test description of order is changed");
		assertEquals(43, order.getLocation(), "Test location of order is changed");
		assertEquals(20.00, order.getPrice(), "Test price of order is changed");
		assertEquals("10:24", order.getStatus(), "Tests the status of the order is changed");

	}
	
	@Test
	void OrderTimeTest() {
		Date now = new java.util.Date();
		Time current = new java.sql.Time(now.getTime());
		order.setOrdertime(current);
		assertEquals(current.getClass(), order.getOrdertime().getClass(), "Ordertime should return the time order is placed");

	}
	

}
