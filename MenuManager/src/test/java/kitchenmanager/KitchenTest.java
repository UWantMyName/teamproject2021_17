package kitchenmanager;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Time;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**Junit Tests for the Kitchen class.
 * @author sebas
 *
 */
class KitchenTest {

	Kitchen kitchen;
	long now = System.currentTimeMillis();
	Time timestamp = new Time(now);

	@BeforeEach
	void setup() {
		kitchen = new Kitchen(1, 12, "3, 5, 7", "recieved", timestamp);
	}

	@Test
	void EmptyConstructtest() {
		Kitchen emptyKitchen = new Kitchen();
		assertEquals(null, emptyKitchen.getDescription(), "Test if a empty kitchen order can be created");
	}

	@Test
	void FilledConstructtest() {
		assertEquals(1, kitchen.getOrderID(), "Test id of kitchen order is constructed correctly");
		assertEquals("recieved", kitchen.getStatus(), "Test name of kitchen order is constructed correctly");
		assertEquals("3, 5, 7", kitchen.getDescription(), "Test description of kitchen order is constructed correctly");
		assertEquals(12, kitchen.getTablenum(), "Test table number of kitchen order is constructed correctly");
		assertEquals(timestamp, kitchen.getOrdertime(),"Test ordertime of kitchen order is constructed correctly");
	}

	@Test
	void EditMenuItemtest() {
		assertEquals(1, kitchen.getOrderID(), "Test id of kitchen order is constructed correctly");
		assertEquals("recieved", kitchen.getStatus(), "Test name of kitchen order is constructed correctly");
		assertEquals("3, 5, 7", kitchen.getDescription(), "Test description of kitchen order is constructed correctly");
		assertEquals(12, kitchen.getTablenum(), "Test table number of kitchen order is constructed correctly");
		assertEquals(timestamp, kitchen.getOrdertime(),"Test ordertime of kitchen order is constructed correctly");

		kitchen.setOrderID(5);
		kitchen.setDescription("The changed description");
		kitchen.setTablenum(43);
		kitchen.setStatus("maybe");

		assertEquals(5, kitchen.getOrderID(), "Test id of kitchen order is changed");
		assertEquals("maybe", kitchen.getStatus(), "Test name of kitchen order is changed");
		assertEquals("The changed description", kitchen.getDescription(),
				"Test description of kitchen order is changed");
		assertEquals(43, kitchen.getTablenum(), "Test table number of kitchen order is changed");
	}

}
