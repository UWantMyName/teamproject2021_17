//package kitchenmanager;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//import java.util.List;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
//class KitchenServiceTest {
//
//	KitchenService service;
//
//	Kitchen kitchen;
//
//	@BeforeEach
//	void setup() {
//		kitchen = new Kitchen(1, 12, "3, 5, 7", "recieved");
//	}
//
//	@Test
//	void SaveGetListtest() {
//		service.save(kitchen);
//		service.save(kitchen);
//		service.save(kitchen);
//		List<Kitchen> kitchenList = service.listAll();
//		assertEquals(kitchenList.size(), 1,
//				"Check if records saved to repo can be retrieved as list using listAll if duplicates then duplicates are not saved");
//	}
//
//	@Test
//	void SaveDeleteItemtest() {
//		service.save(kitchen);
//		Kitchen kitchen1 = new Kitchen(2, 43, "4, 2", "recieved");
//		service.save(kitchen1);
//		List<Kitchen> kitchenList = service.listAll();
//		assertEquals(kitchenList.size(), 2, "Check if records saved to repo can be retrieved as list using listAll");
//		service.delete(1);
//		List<Kitchen> kitchenList1 = service.listAll();
//		assertEquals(kitchenList1.size(), 1,
//				"Check if records saved to repo can be retrieved as list using listAll after deletion");
//	}
//
//}
