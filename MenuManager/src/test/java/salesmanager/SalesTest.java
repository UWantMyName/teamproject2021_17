package salesmanager;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Timestamp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import stockmanager.StockRepository;

/**Junit Tests for the Sales class.
 * @author sebas
 *
 */
public class SalesTest {

	Sales sales;
	StockRepository stocklist;
	Timestamp timestamp = new Timestamp(System.currentTimeMillis());

	@BeforeEach
	void setup() {
		Sales s = new Sales();
		sales = new Sales(timestamp, "3, 5, 7", 1, 127.50);
	}

	@Test
	void EmptyConstructtest() {
		Sales emptysale = new Sales();
		assertEquals(null, emptysale.getDescription(), "Test if a empty sales item can be created");
	}

	@Test
	void FilledConstructtest() {
		assertEquals(timestamp, sales.getOrderdate(), "Test timestamp of sales constructed corretly");
		assertEquals("3, 5, 7", sales.getDescription(), "Test description of sales is constructed correctly");
		assertEquals(127.50, sales.getValue(), "Test value of sales is constructed correctly");
		assertEquals(0, sales.getSaleID(), "Test SalesID is constructed correctly");
		assertEquals(1, sales.getStaffID(), "Test StaffID is constructed correctly");
	}

	@Test
	void EditSalesItemtest() {
		assertEquals(timestamp, sales.getOrderdate(), "Test timestamp of sales constructed corretly");
		assertEquals("3, 5, 7", sales.getDescription(), "Test description of sales is constructed correctly");
		assertEquals(127.50, sales.getValue(), "Test value of sales is constructed correctly");
		assertEquals(0, sales.getSaleID(), "Test SalesID is constructed correctly");
		assertEquals(1, sales.getStaffID(), "Test StaffID is constructed correctly");

		sales.setDescription("2,1,2");
		sales.setValue(220.2);
		sales.setSaleID(5);
		sales.setStaffID(2);

		assertEquals("2,1,2", sales.getDescription(), "Test if description is changed correctly");
		assertEquals(220.2, sales.getValue(), "Test if value is changed correcly");
		assertEquals(5, sales.getSaleID(), "Test if salesID is changed correcly");
		assertEquals(2, sales.getStaffID(), "Test if StaffID is changed correcly");
	}

	@Test
	void SaleTimeTest() {
		sales.setOrderdate(timestamp);
		;
		assertEquals(timestamp.getClass(), sales.getOrderdate().getClass(),
				"sales should return the time sale was made");

	}

}
