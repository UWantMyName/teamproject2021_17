package staffmanager;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**Junit Tests for the staff class.
 * @author sebas
 *
 */
public class staffTest {
	Staff staff;
	StaffRepository staffrep;

	@BeforeEach
	void setup() {
		staff = new Staff(1,"Seba","Waiter","ON");
	}

	@Test
	void EmptyConstructtest() {
		Staff emptystaff = new Staff();
		assertEquals(null, emptystaff.getName(), "Test if a empty staff item can be created");
	}
	void FilledConstructtest() {
		assertEquals(1, staff.getStaffID(), "Test if staffID is constructed correctly");
		assertEquals("Seba", staff.getName(), "Test if Name is constructed correctly");
		assertEquals("Waiter",staff.getRole(), "Test if Role is constructed correctly");
		assertEquals("ON",staff.getOnduty(),"Test if Dutity variable is constructed correctly");
	}
	@Test
	void EditStaffItemtest() {
		assertEquals(1, staff.getStaffID(), "Test if staffID is constructed correctly");
		assertEquals("Seba", staff.getName(), "Test if Name is constructed correctly");
		assertEquals("Waiter",staff.getRole(), "Test if Role is constructed correctly");
		assertEquals("ON",staff.getOnduty(),"Test if Dutity variable is constructed correctly");
		
		staff.setName("Sean");
		staff.setOnduty("OFF");
		staff.setRole("Manager");
		staff.setStaffID(5);

		assertEquals(5,staff.getStaffID(),"Test if StaffID is changed correctly");
		assertEquals("Sean", staff.getName(), "Test if Name is changed correctly");
		assertEquals("OFF", staff.getOnduty(),"Test if Onduty is changed correcly");
		assertEquals("Manager", staff.getRole(),"Test if Role is changed correcly");

	}
}
