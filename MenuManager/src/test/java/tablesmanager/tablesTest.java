package tablesmanager;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import stockmanager.Stock;
import stockmanager.StockRepository;

/**Junit Tests for the tables class.
 * @author sebas
 *
 */
public class tablesTest {

		Tables table;
		TablesRepository tablesrep;

		@BeforeEach
		void setup() {
			table = new Tables(1,1);
		}

		@Test
		void EmptyConstructtest() {
			Tables emptytable = new Tables();
			assertEquals(0, emptytable.getRequestID(), "Test if a empty table item can be created");
		}
		void FilledConstructtest() {
			assertEquals(1,table.getRequestID(),"Test if requestID is constructed correctly");
			assertEquals(1,table.getTablenum(),"Test if tableNumber is constructed correctly");
		}
		@Test
		void EditTableItemtest() {
			assertEquals(1,table.getRequestID(),"Test if requestID is constructed correctly");
			assertEquals(1,table.getTablenum(),"Test if tableNumber is constructed correctly");
			table.setRequestID(5);
			table.setTablenum(50);;
			assertEquals(5,table.getRequestID(),"Test if requestID is set correctly");
			assertEquals(50,table.getTablenum(),"Test if TableNumber is set correctly");
		}

}
