package stockmanager;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import staffmanager.Staff;

/**Junit Tests for the Stock class.
 * @author sebas
 *
 */
public class stockTest {
	Stock stock;
	StockRepository stockrep;

	@BeforeEach
	void setup() {
		stock = new Stock("EGG", 200);
	}

	@Test
	void EmptyConstructtest() {
		Stock emptystock = new Stock();
		assertEquals(null, emptystock.getItemname(), "Test if a empty staff item can be created");
	}

	void FilledConstructtest() {
		assertEquals("EGG",stock.getItemname(),"Test if stockname is constructed correctly");
		assertEquals(200,stock.getQuantity(),"Test if quantity is constructed correctly");
	}
	@Test
	void EditStockItemtest() {
		assertEquals("EGG",stock.getItemname(),"Test if stockname is constructed correctly");
		assertEquals(200,stock.getQuantity(),"Test if quantity is constructed correctly");
		
		stock.setItemname("Bread");
		stock.setQuantity(50);
		assertEquals("Bread",stock.getItemname(),"Test if stockname is set correctly");
		assertEquals(50,stock.getQuantity(),"Test if quantity is set correctly");
	}
}
